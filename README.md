# 学习微服务springcloud基础框架

#### 介绍
这是一个springcloud微服务的广东天气预报系统

#### 软件架构
软件架构说明：
采用springboot+springcloud+redis+html构建微服务案例

#### 业务流程图

1. ![Image text](https://gitee.com/zewen6019/gitee-java/raw/master/image/report.png)







#### 安装教程

1. 开发环境：

   开发工具：
   
   IDEA
   
   JDK1.8
   
   Maven
   
   Redis（作为数据存储）
   
   运行环境：windows 7 or 10
   
2.按照maven方式导入项目

3. 主要技术

   语言：JAVA 
   
3.本地运行把服务全部跑起来，然后登陆：http://localhost:8080/report/cityId/101280101

4.阿里云项目演示地址：http://39.98.184.242:19000/report/cityId/101280101

5.项目演示图：
1. ![Image text](https://gitee.com/zewen6019/gitee-java/raw/master/image/2.png)

2. ![Image text](https://gitee.com/zewen6019/gitee-java/raw/master/image/3.png)






